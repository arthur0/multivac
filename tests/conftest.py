import os

import pytest
from _pytest import monkeypatch

from flask import Flask as _Flask


@pytest.fixture(scope="session", autouse=True)
def _standard_os_environ():
    """Set up ``os.environ`` at the start of the test session to have
    standard values. Returns a list of operations that is used by
    :func:`._reset_os_environ` after each test.
    """
    mp = monkeypatch.MonkeyPatch()
    out = (
        (os.environ, "FLASK_APP", monkeypatch.notset),
        (os.environ, "FLASK_ENV", monkeypatch.notset),
        (os.environ, "FLASK_DEBUG", monkeypatch.notset),
    )

    for _, key, value in out:
        if value is monkeypatch.notset:
            mp.delenv(key, False)
        else:
            mp.setenv(key, value)

    yield out
    mp.undo()


@pytest.fixture(autouse=True)
def _reset_os_environ(monkeypatch, _standard_os_environ):
    """Reset ``os.environ`` to the standard environ after each test,
    in case a test changed something without cleaning up.
    """
    monkeypatch._setitem.extend(_standard_os_environ)


class Flask(_Flask):
    testing = True
    secret_key = "test key"


@pytest.fixture
def app():
    app = Flask("flask_test", root_path=os.path.dirname(__file__))
    return app


@pytest.fixture
def app_ctx(app):
    with app.app_context() as ctx:
        yield ctx


@pytest.fixture
def req_ctx(app):
    with app.test_request_context() as ctx:
        yield ctx


@pytest.fixture
def client(app):
    return app.test_client()
