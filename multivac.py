"""
CLI Tool
"""
from dynaconf import FlaskDynaconf
from rq import Connection, Queue, Worker

from app import create_app, redis_db

app = create_app()
FlaskDynaconf(app)


@app.cli.command("work")
def work():
    'Starts redis worker loop.'
    listen = ['default']
    redis_conn = redis_db.get_connection()
    with Connection(redis_conn):
        worker = Worker(map(Queue, listen))
        worker.work()

app.cli.add_command(work)
