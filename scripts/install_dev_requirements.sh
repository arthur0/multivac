#!/usr/bin/env bash
source ./utils.sh

preflight_checks
update_packages

deps=(
  autoconf
  autoconf-archive
  automake
  build-essential
  g++
  gcc
  git
  libcmocka-dev
  libcmocka0
  libcurl4 
  libgcrypt20-dev
  libssl-dev
  libtool
  liburiparser-dev
  lnav
  m4
  openssl
  pkg-config
)

sudo apt-get install -y ${deps[@]}
