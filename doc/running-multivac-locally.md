## Running MultiVAC locally

The MultiVAC is designed to run into containers, however you can run it on your local environment.

This tutorial is intended to help you to understand the needed steps to setting up and run the MultiVAC.

> **Note**: As you are using our local machine, you should be comfortable to work using multiple terminals.


### Environment

This tutorial was tested on **Ubuntu 16.04 and 18.04** servers.

If you have other environment, the scripts can can fail but you will find links to official documentation bellow.

### Dev Requirements

Install from [script](../scripts/install_dev_requirements.sh):

```bash
# Get MultiVAC:
git clone https://gitlab.com/arthur0/multivac.git


# Install dev requirements
cd multivac/scripts
 . install_dev_requirements.sh
```

### Setting up MongoDB

Install MongoDB from [script](../scripts/install_mongo.sh):

```bash
. install_mongo.sh
```

> **Note**: [Installation docs](https://docs.mongodb.com/manual/installation/).


Configuring MongoDB according the MultiVAC environment:

```bash
# Considering the Mongo URI: 'mongodb://multivac:isaacasimov@localhost:27017/multivac'
sudo service mongod start

mongo --host 127.0.0.1:27017
# MongoDB outputs [...]

# Use the multivac database
> use multivac
switched to db multivac

# Create the multivac user
> db.createUser({user: "multivac", pwd: "isaacasimov", roles: ["readWrite", "dbAdmin"]})
Successfully added user: { "user" : "multivac", "roles" : [ "readWrite", "dbAdmin" ] }
```

### Setting up Redis

Install from [script](../scripts/install_redis.sh):

```bash
# Let mongo running and open other terminal
. install_redis.sh
```

> **Note**: [Quickstart docs](https://redis.io/topics/quickstart).

To run the Redis server:
```bash
# Example of a Redis config file
cat <<EOF >>redis.conf
maxmemory 20mb
maxmemory-policy allkeys-lru
maxmemory-samples 5
loglevel notice
logfile ""
requirepass isaacasimov
EOF

redis-server redis.conf
```

### Setting up MultiVAC

Running server:

```bash
# Create the environment (on multivac/ directory)
pipenv shell

# Install dependencies from Pipfile
pipenv install

# Export the python module that contains the CLI tool 
export FLASK_APP='multivac.py'

# Help message
flask
usage: manage.py [-?] {worker,list_routes,test,runserver,shell} ...
flask
Usage: flask [OPTIONS] COMMAND [ARGS]...

  A general utility script for Flask applications.

  Provides commands from Flask, extensions, and the application. Loads the
  application defined in the FLASK_APP environment variable, or from a
  wsgi.py file. Setting the FLASK_ENV environment variable to 'development'
  will enable debug mode.

    $ export FLASK_APP=hello.py
    $ export FLASK_ENV=development
    $ flask run

Options:
  --version  Show the flask version
  --help     Show this message and exit.

Commands:
  routes  Show the routes for the app.
  run     Run a development server.
  shell   Run a shell in the app context.
  work    Starts redis worker loop.

# environment (development, production or testing)
export FLASK_ENV=testing

# Run a development server (other options in the help message above).
flask run
```

### Testing MultiVAC
```bash
# It in another terminal:
curl http://127.0.0.1:5000/
Welcome to MultiVAC!

# Ask the Question: How can the entropy of the universe be reversed?
 curl \
    -X GET \
    http://127.0.0.1:5000/multivac
INSUFFICIENT DATA FOR MEANINGFUL ANSWER.

# Adding data=fiat lux to multivac
 curl \
    -X POST \
    -F "data=fiat lux" \
    http://127.0.0.1:5000/multivac/data

# The request above is enqueued, so no data is saved on entropy collection
 curl \
    -X GET \
    http://127.0.0.1:5000/multivac
INSUFFICIENT DATA FOR MEANINGFUL ANSWER.
```

Start RQ worker process and perform the queued job (add the 'data=fiat lux' to entropy collection)

```bash
# Open the multivac environment (on multivac/ directory)
Pipenv shell

# Start worker process
flask work
```

When the worker process starts you should see the queued job done (save data=fiat lux in the entropy collection):

```markdown
# Redis Queue output
RQ worker 'rq:worker:id' started, version 1.0
*** Listening on default...
Cleaning registries for queue: default
default: app.entropy_queue.process_data('fiat lux')
default: Job OK
Result is kept for 500 seconds

# Ask the Question again: How can the entropy of the universe be reversed?  
 curl \
    -X GET \
    http://127.0.0.1:5000/multivac
fiat lux
```
