# Setting up MultiVAC on Kubernetes 
<!-- TODO: Explain each one manifest -->
You only need apply manifests on [k8s folder](../k8s/)

### Deploy Mongo

```bash
kubectl apply -f mongo-service.yaml

# Using statefulset to persistent storage'
kubectl apply -f mongo-statefulset.yaml
```

### Deploy Redis

```bash
# Creating a secret
kubectl create secret generic multivac-secret --from-literal=password="isaacasimov" --from-literal=secret-key="foundation" 

# For all redis configuration see this doc:
# https://github.com/antirez/redis/blob/unstable/redis.conf
kubectl apply -f redis-settings-cm.yaml

kubectl apply -f redis-service.yaml

kubectl apply -f redis-deploy.yaml
```


### Deploy MultiVAC
```bash
kubectl apply -f multivac-settings-cm.yaml

# Finnaly, you can create the MultiVAC Deployment and Service
kubectl apply -f multivac-service.yaml
kubectl apply -f multivac-deploy.yaml
```

### Testing Multivac
```bash
# --> Use label selectors!

# Get all Multivac related resources (including db and cache)
kubectl get all -l=app=multivac
# Get all Multivac db (mongo) related resources
kubectl get all -l=app=multivac,role=db
# Get all Multivac jobs (redis) related resources
kubectl get all -l=app=multivac,role=redis
# Get all Multivac web-server (flask) related resources
kubectl get all -l=app=multivac,role=web-server

# --> Logging: Use label selectors!
kubectl logs -l=app=multivac 

# --> Perform requests
# Util tool
kubectl run dnstools --image=radial/busyboxplus:curl -i --tty
> curl multivac-web-server:5000
Welcome to Multivac!

> curl multivac-web-server:5000/multivac
INSUFFICIENT DATA FOR MEANINGFUL ANSWER.

> curl -X POST -F 'data=fiat lux' multivac-web-server:5000/multivac/data
{"response": "MultiVAC updated!"}

> curl multivac-web-server:5000/multivac
INSUFFICIENT DATA FOR MEANINGFUL ANSWER.
> curl multivac-web-server:5000/multivac
INSUFFICIENT DATA FOR MEANINGFUL ANSWER.
# [...] Ctrl+D

# Revert the entropy process is hard, takes a long time
# So, the MultiVAC worker job role is is to advance it.
# It's a kind of time machine.
kubectl apply -f multivac-worker.yaml

# Testing again
kubectl attach <your dns tools pod> -c dnstools -i -t

# \o/
> curl multivac-web-server:5000/multivac
fiat lux
```