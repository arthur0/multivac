# Using lightweight alpine image
FROM python:3.7-alpine

LABEL maintainer="Arthur Miranda <arthur.souzamiranda@gmail.com"

COPY ./requirements.txt /multivac/requirements.txt

WORKDIR /multivac

RUN pip install -r requirements.txt

COPY . /multivac

ENV FLASK_APP='multivac.py'

EXPOSE 5000

ENTRYPOINT [ "flask" ]
