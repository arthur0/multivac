# MultiVAC

This project is intended to guide Python developers help the humans to colonize the universe. 

To do that, we will use awesome technologies like Kubernetes, Flask, MongoDB and Redis.

## Context

A multi-purpose self-adjusting and self-correcting computer called MultiVAC is 
being developed to help us understand how to reverse the universe's 
[entropy](https://en.wikipedia.org/wiki/Entropy).

If you're confusing with the description above, see the [reference](#reference).

## Overview

Our MultiVAC is currently in the form of a single application that uses a 
***Web API*** through ***HTTP*** with a limited number of ***Endpoints***.

This MultiVAC version can answer only one question:
```
* How can the entropy of the universe be reversed?
```

To do that, the user can perform two possible actions:
```
* Add new data
* Ask the question
```

***Endpoint mapping***

| Function          | Method | Endpoint       | Body Params         |
|-------------------|:-------|:---------------|:-------------------:|
| Add new data      |  POST  | /multivac/data | data: String (form) |
| Ask the question  |  GET   | /multivac      |     -               |

## Running MultiVAC

Before start, [there](./doc/dependency-management.md) is a doc to explain how the dependencies are managed in this project.

After that, if you're a developer, you can [run MultiVAC locally](./doc/running-multivac-locally.md).

It is also possible to [deploy MultiVAC in a Kubernetes cluster](./docs/running-multivac-on-k8s.md).

## Reference

"The Last Question" is a science fiction short story by American writer Isaac Asimov.

The story deals with the development of a series of computers that helps humans to 
colonize the universe.

Are you curious? see the [full text](http://www.multivax.com/last_question.html).