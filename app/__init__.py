"""
Application Factory to create a Flask instance
"""
import os

from flask import Flask


def create_app(test_config=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    from app import multivac_bp
    app.register_blueprint(multivac_bp.bp)
    return app
