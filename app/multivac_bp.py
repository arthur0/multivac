"""
Represents the multivac blueprint, a collection of routes and other
app-related functions that can be registered on a real application
later.
"""
import json

from flask import Blueprint, Response, request
from rq import Queue

from app import entropy_job, mongo_db, redis_db

bp = Blueprint('multivac', __name__)


@bp.route("/", methods=["GET"])
def index():
    return "Welcome to Multivac!"


@bp.route("/multivac", methods=['GET'])
def get_multivac():
    db = mongo_db.get_multivac_db()
    if db.entropy.count() == 0:
        return "INSUFFICIENT DATA FOR MEANINGFUL ANSWER."
    else:
        answer = db.entropy.find_one()['data']
        return str(answer)


@bp.route("/multivac/data", methods=['POST'])
def post_multivac():
    redis_conn = redis_db.get_connection()
    value = request.form['data']

    q = Queue("default", connection=redis_conn)
    q.enqueue_call(func=entropy_job.process_data, args=(value, ))

    return Response(response=json.dumps({"response": "MultiVAC updated!"}),
                    status=200, mimetype="application/json")
