"""
Module to create a job that saves data on entropy collection
"""
import logging

from app import mongo_db

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)
db = mongo_db.get_multivac_db()


def process_data(value):
    log.debug('Queue entropy: Processing data...')
    db.entropy.insert_one({'data': value})
